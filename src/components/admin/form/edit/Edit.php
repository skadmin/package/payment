<?php

declare(strict_types=1);

namespace Skadmin\Payment\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Payment\BaseControl;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\Gateway\GatewayFactory;
use SkadminUtils\Payment\Factory\PaymentSettingsFactory;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function trim;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private PaymentSettingsFactory $paymentSettings;
    private GatewayFactory         $factoryGateway;
    private PaymentFacade          $facade;
    private LoaderFactory          $webLoader;
    private Payment                $payment;

    public function __construct(?int $id, PaymentSettingsFactory $paymentSettings, GatewayFactory $factoryGateway, PaymentFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user)
    {
        parent::__construct($translator, $user);

        $this->paymentSettings = $paymentSettings;
        $this->factoryGateway  = $factoryGateway;
        $this->facade          = $facade;
        $this->webLoader       = $webLoader;

        $this->payment = $this->facade->get($id);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->payment->isLoaded()) {
            return new SimpleTranslation('payment.edit.title - %s', $this->payment->getName());
        }

        return 'payment.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->payment         = $this->payment;
        $template->paymentSettings = $this->paymentSettings;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataGatewayCode = $this->factoryGateway->getAvailableCodes();

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.payment.edit.name')
            ->setRequired('form.payment.edit.name.req');
        $form->addCheckbox('isActive', 'form.payment.edit.is-active')
            ->setDefaultValue(true);
        $form->addSelect('code', 'form.payment.edit.code', $dataGatewayCode)
            ->setTranslator(null)
            ->setPrompt(Constant::PROMTP);
        $form->addCheckbox('isForUnpaid', 'form.payment.edit.is-for-unpaid');
        $form->addImageWithRFM('imagePreview', 'form.payment.edit.image-preview');

        // TEXT
        $form->addTextArea('content', 'form.payment.edit.content');
        $form->addTextArea('contentEmail', 'form.payment.edit.content-email');

        // BUTTON
        $form->addSubmit('send', 'form.payment.edit.send');
        $form->addSubmit('sendBack', 'form.payment.edit.send-back');
        $form->addSubmit('back', 'form.payment.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->payment->isLoaded()) {
            $payment = $this->facade->update(
                $this->payment->getId(),
                $values->name,
                $values->isActive,
                $values->content,
                $values->contentEmail,
                $values->code,
                $values->isForUnpaid,
                $identifier
            );
            $this->onFlashmessage('form.payment.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $payment = $this->facade->create(
                $values->name,
                $values->isActive,
                $values->content,
                $values->contentEmail,
                $values->code,
                $values->isForUnpaid,
                $identifier
            );
            $this->onFlashmessage('form.payment.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $payment->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->payment->isLoaded()) {
            return [];
        }

        return [
            'name'         => $this->payment->getName(),
            'content'      => $this->payment->getContent(),
            'contentEmail' => $this->payment->getContentEmail(),
            'isActive'     => $this->payment->isActive(),
            'isForUnpaid'  => $this->payment->isForUnpaid(),
            'code'         => trim($this->payment->getCode()) === '' ? null : $this->payment->getCode(),
        ];
    }
}
