<?php

declare(strict_types=1);

namespace Skadmin\Payment\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Payment\BaseControl;
use Skadmin\Payment\Doctrine\Payment\Payment;
use Skadmin\Payment\Doctrine\Payment\PaymentFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\Gateway\GatewayFactory;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;

use function sprintf;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private PaymentFacade  $facade;
    private GatewayFactory $gatewayFactory;
    private ImageStorage   $imageStorage;

    public function __construct(GatewayFactory $gatewayFactory, PaymentFacade $facade, Translator $translator, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade         = $facade;
        $this->gatewayFactory = $gatewayFactory;
        $this->imageStorage   = $imageStorage;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'payment.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Payment $payment): ?Html {
                if ($payment->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$payment->getImagePreview(), '55x55', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');
        $grid->addColumnText('name', 'grid.payment.overview.name')
            ->setRenderer(function (Payment $payment): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $payment->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($payment->getName());

                return $name;
            });
        $grid->addColumnText('code', 'grid.payment.overview.code')
            ->setReplacement($this->gatewayFactory->getAvailableCodes());
        $this->addColumnIsActive($grid, 'payment.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.payment.overview.name', ['name']);
        $this->addFilterIsActive($grid, 'payment.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.payment.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.payment.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // OTHER
        $grid->setDefaultSort(['name' => 'ASC']);

        return $grid;
    }
}
