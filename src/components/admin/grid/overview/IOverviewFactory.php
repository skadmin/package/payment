<?php

declare(strict_types=1);

namespace Skadmin\Payment\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
