<?php

declare(strict_types=1);

namespace Skadmin\Payment\Doctrine\Payment;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class PaymentFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Payment::class;
    }

    public function create(string $name, bool $isActive, string $content, string $contentEmail, ?string $code, bool $isForUnpaid, ?string $imagePreview): Payment
    {
        return $this->update(null, $name, $isActive, $content, $contentEmail, $code, $isForUnpaid, $imagePreview);
    }

    public function update(?int $id, string $name, bool $isActive, string $content, string $contentEmail, ?string $code, bool $isForUnpaid, ?string $imagePreview): Payment
    {
        $payment = $this->get($id);
        $payment->update($name, $isActive, $content, $contentEmail, $code, $isForUnpaid, $imagePreview);

        // update isForUnpaid to false exlude current id
        if ($isForUnpaid) {
            foreach ($this->getAllForUnpaid() as $unpaidPayment) {
                if ($unpaidPayment->getId() === $id) {
                    continue;
                }

                $unpaidPayment->setIsForUnpaid(false);
                $this->em->persist($unpaidPayment);
            }
        }

        $this->em->persist($payment);
        $this->em->flush();

        return $payment;
    }

    public function get(?int $id = null): Payment
    {
        if ($id === null) {
            return new Payment();
        }

        $payment = parent::get($id);

        if ($payment === null) {
            return new Payment();
        }

        return $payment;
    }

    /**
     * @return Payment[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    /**
     * @return Payment[]
     */
    public function getAllForUnpaid(): array
    {
        $criteria = ['isForUnpaid' => true];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    public function getForUnpaid(): Payment
    {
        $criteria = ['isForUnpaid' => true];

        $payment = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $payment ?? $this->getAll(true)[0];
    }
}
