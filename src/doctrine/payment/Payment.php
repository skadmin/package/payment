<?php

declare(strict_types=1);

namespace Skadmin\Payment\Doctrine\Payment;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function trim;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Payment
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\Code;
    use Entity\ImagePreview;

    #[ORM\Column(type: Types::TEXT)]
    private string $contentEmail;

    #[ORM\Column(options: ['default' => false])]
    private bool $isForUnpaid = false;

    public function update(string $name, bool $isActive, string $content, string $contentEmail, ?string $code, bool $isForUnpaid, ?string $imagePreview): void
    {
        $this->name         = $name;
        $this->isActive     = $isActive;
        $this->content      = $content;
        $this->contentEmail = $contentEmail;
        $this->code         = $code ?? '';

        $this->setIsForUnpaid($isForUnpaid);

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function isForUnpaid(): bool
    {
        return $this->isForUnpaid;
    }

    public function setIsForUnpaid(bool $isForUnpaid): void
    {
        $this->isForUnpaid = $isForUnpaid;
    }

    public function existCode(): bool
    {
        return $this->code !== null && trim($this->code) !== '';
    }

    public function getContentEmail(): string
    {
        return $this->contentEmail;
    }
}
