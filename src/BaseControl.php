<?php

declare(strict_types=1);

namespace Skadmin\Payment;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'payment';
    public const DIR_IMAGE = 'payment';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-file-invoice-dollar']),
            'items'   => ['overview'],
        ]);
    }
}
