<?php

declare(strict_types=1);

namespace SkadminUtils\Payment\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\Payment\Factory\PaymentSettingsFactory;

class PaymentExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'allowContentEmail' => Expect::bool(false),
        ]);
    }

    public function loadConfiguration(): void
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $builder->addDefinition($this->prefix('paymentSettingsFactory'))
            ->setClass(PaymentSettingsFactory::class)
            ->setArguments([
                $this->config->allowContentEmail,
            ]);
    }
}
