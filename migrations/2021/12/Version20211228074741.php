<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211228074741 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE payment ADD image_preview VARCHAR(255) DEFAULT NULL');

        $translations = [
            ['original' => 'form.payment.edit.image-preview', 'hash' => 'b6fa264475aa2e3829cd508287028ee4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Logo', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.image-preview.rule-image', 'hash' => '169d283058c10e642d36ff3341b6d9cd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE payment DROP image_preview');
    }
}
