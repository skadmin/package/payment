<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316155221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'payment.overview', 'hash' => 'c36b73e56e94971c324626b03f8c06df', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy plateb', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.payment.title', 'hash' => '02919eef187fc27763516e37158d1fe5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy plateb', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.payment.description', 'hash' => 'a7b28e75a02c5d38983634825a5eb16f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat typy plateb', 'plural1' => '', 'plural2' => ''],
            ['original' => 'payment.overview.title', 'hash' => '01bbfc16de7ce411dc4ee3eab5309d23', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typy plateb|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.payment.overview.is-active', 'hash' => 'd8c89886e212c1ed4da07b89adec937c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.payment.overview.action.new', 'hash' => 'ab85a8d0aa4d7243e477bc09c8db7f2a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit typ platby', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.payment.overview.name', 'hash' => '0972d9659dba6b3f8dd2e93f33d2e342', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.payment.overview.code', 'hash' => '8a8d87a775d55a006a4a8a49bd7cd343', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Napojení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'payment.edit.title', 'hash' => '0f742b64dc8262af8140c831e212547d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení typu platby', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.name', 'hash' => '5ff1d01877e31e2014fce234595cad8a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.name.req', 'hash' => '24fb51c2fcabb67e5bff86a76f4e3cfc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.code', 'hash' => '1ee48723b41cec782e52b794f1db6f64', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Napojení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.content', 'hash' => '45c2f4ac07b24597126bfeeb02189e71', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.is-active', 'hash' => 'f5fc7406824c75486fed17bb5ca8d29d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.is-for-unpaid', 'hash' => 'f911bfd4522e64e2fdfb5c3fa9703ab5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pro platby zdarma', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.send', 'hash' => 'd4cdabc7cae79c7766d076ff8e3c38d2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.send-back', 'hash' => '4aea457e768b5e06fa1be5c0a7053586', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.back', 'hash' => '6a2488e6e167a21339301f3d7d6c05bb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'payment.edit.title - %s', 'hash' => '00c6a34a7c41df1687641a32460e9c53', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace typu platby', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.flash.success.create', 'hash' => 'dfa97f58b5058c75eb84ae0fd9016ba3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ platby byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.payment.edit.flash.success.update', 'hash' => 'f65aadc14ec85f3bf03141207ceb8faa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ platby byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.payment.overview.action.edit', 'hash' => '9faf74cc8c2c7687046659a5ad3c9cec', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
